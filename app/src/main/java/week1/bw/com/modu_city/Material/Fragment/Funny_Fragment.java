package week1.bw.com.modu_city.Material.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.util.ArrayList;
import java.util.List;

import week1.bw.com.modu_city.Material.Adapter.MyAdapter;
import week1.bw.com.modu_city.Material.Enity.Bean;
import week1.bw.com.modu_city.R;

public class Funny_Fragment extends Fragment implements XRecyclerView.LoadingListener{
    private View view;
    private XRecyclerView recyclerView;
    private List<Bean.DataBean> list = new ArrayList<>();
    private String url = "{\"ret\":1,\"data\":[{\"id\":\"8289\",\"title\":\"\\u6cb9\\u7116\\u5927\\u867e\",\"pic\":\"http:\\/\\/www.qubaobei.com\\/ios\\/cf\\/uploadfile\\/132\\/9\\/8289.jpg\",\"collect_num\":\"1667\",\"food_str\":\"\\u5927\\u867e \\u8471 \\u751f\\u59dc \\u690d\\u7269\\u6cb9 \\u6599\\u9152\",\"num\":1667},{\"id\":\"2127\",\"title\":\"\\u56db\\u5ddd\\u56de\\u9505\\u8089\",\"pic\":\"http:\\/\\/www.qubaobei.com\\/ios\\/cf\\/uploadfile\\/132\\/3\\/2127.jpg\",\"collect_num\":\"1590\",\"food_str\":\"\\u732a\\u8089 \\u9752\\u849c \\u9752\\u6912 \\u7ea2\\u6912 \\u59dc\\u7247\",\"num\":1590},{\"id\":\"30630\",\"title\":\"\\u8d85\\u7b80\\u5355\\u8292\\u679c\\u5e03\\u4e01\",\"pic\":\"http:\\/\\/www.qubaobei.com\\/ios\\/cf\\/uploadfile\\/132\\/31\\/30630.jpg\",\"collect_num\":\"1538\",\"food_str\":\"QQ\\u7cd6 \\u725b\\u5976 \\u8292\\u679c\",\"num\":1538},{\"id\":\"9073\",\"title\":\"\\u5bb6\\u5e38\\u7ea2\\u70e7\\u9c7c\",\"pic\":\"http:\\/\\/www.qubaobei.com\\/ios\\/cf\\/uploadfile\\/132\\/10\\/9073.jpg\",\"collect_num\":\"1424\",\"food_str\":\"\\u9c9c\\u9c7c \\u59dc \\u8471 \\u849c \\u82b1\\u6912\",\"num\":1424},{\"id\":\"10097\",\"title\":\"\\u5bb6\\u5e38\\u714e\\u8c46\\u8150\",\"pic\":\"http:\\/\\/www.qubaobei.com\\/ios\\/cf\\/uploadfile\\/132\\/11\\/10097.jpg\",\"collect_num\":\"1417\",\"food_str\":\"\\u8c46\\u8150 \\u65b0\\u9c9c\\u7ea2\\u6912 \\u9752\\u6912 \\u8471\\u82b1 \\u6cb9\",\"num\":1417},{\"id\":\"10509\",\"title\":\"\\u6c34\\u716e\\u8089\\u7247\",\"pic\":\"http:\\/\\/www.qubaobei.com\\/ios\\/cf\\/uploadfile\\/132\\/11\\/10509.jpg\",\"collect_num\":\"1339\",\"food_str\":\"\\u7626\\u732a\\u8089 \\u751f\\u83dc \\u8c46\\u74e3\\u9171 \\u5e72\\u8fa3\\u6912 \\u82b1\\u6912\",\"num\":1339},{\"id\":\"46968\",\"title\":\"\\u7ea2\\u7cd6\\u82f9\\u679c\\u94f6\\u8033\\u6c64\",\"pic\":\"http:\\/\\/www.qubaobei.com\\/ios\\/cf\\/uploadfile\\/132\\/47\\/46968.jpg\",\"collect_num\":\"1252\",\"food_str\":\"\\u94f6\\u8033 \\u82f9\\u679c \\u7ea2\\u7cd6\",\"num\":1252},{\"id\":\"10191\",\"title\":\"\\u9ebb\\u5a46\\u8c46\\u8150\",\"pic\":\"http:\\/\\/www.qubaobei.com\\/ios\\/cf\\/uploadfile\\/132\\/11\\/10191.jpg\",\"collect_num\":\"1220\",\"food_str\":\"\\u8c46\\u8150 \\u8089\\u672b \\u751f\\u62bd \\u767d\\u7cd6 \\u829d\\u9ebb\\u6cb9\",\"num\":1220},{\"id\":\"2372\",\"title\":\"\\u76ae\\u86cb\\u7626\\u8089\\u7ca5\",\"pic\":\"http:\\/\\/www.qubaobei.com\\/ios\\/cf\\/uploadfile\\/132\\/3\\/2372.jpg\",\"collect_num\":\"1151\",\"food_str\":\"\\u5927\\u7c73 \\u76ae\\u86cb \\u732a\\u8089 \\u6cb9\\u6761 \\u9999\\u8471\",\"num\":1151},{\"id\":\"2166\",\"title\":\"\\u8682\\u8681\\u4e0a\\u6811\",\"pic\":\"http:\\/\\/www.qubaobei.com\\/ios\\/cf\\/uploadfile\\/132\\/3\\/2166.jpg\",\"collect_num\":\"1143\",\"food_str\":\"\\u7ea2\\u85af\\u7c89 \\u8089 \\u59dc \\u849c \\u82b1\\u6912\",\"num\":1143},{\"id\":\"2262\",\"title\":\"\\u7cd6\\u918b\\u8089\",\"pic\":\"http:\\/\\/www.qubaobei.com\\/ios\\/cf\\/uploadfile\\/132\\/3\\/2262.jpg\",\"collect_num\":\"1077\",\"food_str\":\"\\u732a\\u8089 \\u7ea2\\u6912 \\u9ec4\\u6912 \\u6d0b\\u8471 \\u86cb\\u6e05\",\"num\":1077},{\"id\":\"9971\",\"title\":\"\\u9c7c\\u9999\\u8c46\\u8150\",\"pic\":\"http:\\/\\/www.qubaobei.com\\/ios\\/cf\\/uploadfile\\/132\\/10\\/9971.jpg\",\"collect_num\":\"1009\",\"food_str\":\"\\u8c46\\u8150 \\u6728\\u8033 \\u80e1\\u841d\\u535c \\u9999\\u8471 \\u756a\\u8304\\u9171\",\"num\":1009},{\"id\":\"10172\",\"title\":\"\\u5e72\\u7178\\u56db\\u5b63\\u8c46\",\"pic\":\"http:\\/\\/www.qubaobei.com\\/ios\\/cf\\/uploadfile\\/132\\/11\\/10172.jpg\",\"collect_num\":\"991\",\"food_str\":\"\\u56db\\u5b63\\u8c46 \\u5e72\\u8fa3\\u6912 \\u849c\\u5934 \\u9171\\u6cb9 \\u7cd6\",\"num\":991},{\"id\":\"2685\",\"title\":\"\\u80e1\\u841d\\u535c\\u8089\\u672b\\u84b8\\u86cb\",\"pic\":\"http:\\/\\/www.qubaobei.com\\/ios\\/cf\\/uploadfile\\/132\\/3\\/2685.jpg\",\"collect_num\":\"926\",\"food_str\":\"\\u80e1\\u841d\\u535c \\u8089 \\u86cb \\u751f\\u62bd \\u76d0\",\"num\":926},{\"id\":\"9972\",\"title\":\"\\u864e\\u76ae\\u9752\\u6912\",\"pic\":\"http:\\/\\/www.qubaobei.com\\/ios\\/cf\\/uploadfile\\/132\\/10\\/9972.jpg\",\"collect_num\":\"891\",\"food_str\":\"\\u9752\\u8fa3\\u6912 \\u5927\\u849c \\u9999\\u918b \\u767d\\u7cd6 \\u751f\\u62bd\",\"num\":891},{\"id\":\"10437\",\"title\":\"\\u53c9\\u70e7\\u6392\\u9aa8\",\"pic\":\"http:\\/\\/www.qubaobei.com\\/ios\\/cf\\/uploadfile\\/132\\/11\\/10437.jpg\",\"collect_num\":\"801\",\"food_str\":\"\\u6392\\u9aa8 \\u674e\\u9526\\u8bb0\\u53c9\\u70e7\\u9171 \\u690d\\u7269\\u6cb9 \\u6e05\\u6c34 \\u6cb9\\u83dc\",\"num\":801},{\"id\":\"2892\",\"title\":\"\\u201c\\u4e94\\u884c\\u201d\\u5f69\\u852c\\u6c64\",\"pic\":\"http:\\/\\/www.qubaobei.com\\/ios\\/cf\\/uploadfile\\/132\\/3\\/2892.jpg\",\"collect_num\":\"760\",\"food_str\":\"\\u9ed1\\u6728\\u8033 \\u7389\\u7c73 \\u725b\\u84a1 \\u80e1\\u841d\\u535c \\u897f\\u5170\\u82b1\",\"num\":760},{\"id\":\"33783\",\"title\":\"\\u7f8e\\u4eba\\u8c46\\u6d46\",\"pic\":\"http:\\/\\/www.qubaobei.com\\/ios\\/cf\\/uploadfile\\/132\\/34\\/33783.jpg\",\"collect_num\":\"757\",\"food_str\":\"\\u9ec4\\u8c46 \\u7ea2\\u8c46 \\u7eff\\u8c46 \\u9ed1\\u8c46 \\u9ed1\\u7c73\",\"num\":757},{\"id\":\"2348\",\"title\":\"\\u9ebb\\u8fa3\\u8089\\u4e1d\\u9762\",\"pic\":\"http:\\/\\/www.qubaobei.com\\/ios\\/cf\\/uploadfile\\/132\\/3\\/2348.jpg\",\"collect_num\":\"756\",\"food_str\":\"\\u9762\\u6761 \\u8089\\u4e1d \\u6dc0\\u7c89 \\u9171\\u6cb9 \\u8fa3\\u6912\",\"num\":756},{\"id\":\"10044\",\"title\":\"\\u571f\\u8c46\\u7096\\u7fc5\\u6839\",\"pic\":\"http:\\/\\/www.qubaobei.com\\/ios\\/cf\\/uploadfile\\/132\\/11\\/10044.jpg\",\"collect_num\":\"754\",\"food_str\":\"\\u571f\\u8c46 \\u7fc5\\u6839 \\u8471 \\u59dc \\u6599\\u9152\",\"num\":754}]}";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.funny_fragment, null);
        initData();
        init();
        return view;
    }

    private void init(){
        recyclerView = view.findViewById(R.id.funny_mXRv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        MyAdapter myAdapter = new MyAdapter(list,"搞笑");
        myAdapter.setMyAdapter(myAdapter);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLoadingListener(this);
    }


    private void initData(){
        Bean bean = new Gson().fromJson(url,Bean.class);
        list = bean.getData();
    }

    @Override
    public void onRefresh() {
        recyclerView.refreshComplete();
    }

    @Override
    public void onLoadMore() {
        recyclerView.loadMoreComplete();
    }
}
