package week1.bw.com.modu_city.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;

import week1.bw.com.modu_city.R;


public class DetailsActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView back,close;
    private WebView webView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_activity);
        init();
    }

    private void init(){
        back = findViewById(R.id.details_back);
        close = findViewById(R.id.details_close);
        webView = findViewById(R.id.details_webview);

        back.setOnClickListener(this);
        close.setOnClickListener(this);
        webView.loadUrl("http://mini.eastday.com/a/190412170246983.html?qid=02157");
        WebSettings webSettings = webView.getSettings();
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
    }

    @Override
    public void onClick(View view) {
        if (R.id.details_back == view.getId()) {
            finish();
        } else if (R.id.details_close == view.getId()) {
            finish();
        }
    }
}
