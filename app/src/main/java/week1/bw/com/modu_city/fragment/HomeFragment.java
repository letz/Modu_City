package week1.bw.com.modu_city.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.util.List;

import week1.bw.com.modu_city.IGetData;
import week1.bw.com.modu_city.R;
import week1.bw.com.modu_city.adapter.HomeFragmentAdapter;
import week1.bw.com.modu_city.base.BaseFragment;
import week1.bw.com.modu_city.entity.FoodBean;
import week1.bw.com.modu_city.net.RetrofitGetData;

public class HomeFragment extends BaseFragment implements IGetData {
    private RetrofitGetData retrofitGetData;
    private XRecyclerView rv;
    private List<FoodBean.DataBean> data;

    @Override
    public View initView() {
        View inflate = LayoutInflater.from(mContext).inflate(R.layout.fragment_home, null);
        rv = inflate.findViewById(R.id.rv);

        retrofitGetData = new RetrofitGetData(this);
        retrofitGetData.getDataAsync();

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(manager);
//        rv.setRefreshHeader();
        rv.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {

            }

            @Override
            public void onLoadMore() {

            }
        });

        return inflate;
    }

    @Override
    public void initData() {
        super.initData();
    }

    @Override
    public void getString(FoodBean foodBean) {
//        data = foodBean.getData();
        rv.setAdapter(new HomeFragmentAdapter(getContext(),foodBean.getData()));
    }
}
