package week1.bw.com.modu_city.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import week1.bw.com.modu_city.R;
import week1.bw.com.modu_city.entity.FoodBean;

public class ChannelAdapter extends BaseAdapter {
    private Context mContext;
    private List<FoodBean.DataBean> data;

    public ChannelAdapter(Context mContext, List<FoodBean.DataBean> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_channel, parent, false);
            holder = new ViewHolder();
            holder.ivChannel = convertView.findViewById(R.id.iv_channel);
            holder.tvChannel = convertView.findViewById(R.id.tv_channel);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        Glide.with(mContext).load(data.get(position).getPic()).into(holder.ivChannel);
        holder.tvChannel.setText(data.get(position).getTitle());
        return convertView;
    }

    class ViewHolder{
        ImageView ivChannel;
        TextView tvChannel;
    }
}
