package week1.bw.com.modu_city.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import week1.bw.com.modu_city.R;
import week1.bw.com.modu_city.fragment.HomeFragment;
import week1.bw.com.modu_city.fragment.MaterialFragment;
import week1.bw.com.modu_city.fragment.MineFragment;
import week1.bw.com.modu_city.fragment.ShopFragment;


public class MainActivity extends AppCompatActivity {

    private FrameLayout pageFragment;
    private RadioButton homepage;
    private RadioButton shop;
    private RadioButton material;
    private RadioButton mine;
    private RadioGroup bottomLab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        pageFragment = (FrameLayout) findViewById(R.id.pageFragment);
        homepage = (RadioButton) findViewById(R.id.homepage);
        shop = (RadioButton) findViewById(R.id.shop);
        material = (RadioButton) findViewById(R.id.material);
        mine = (RadioButton) findViewById(R.id.mine);
        bottomLab = (RadioGroup) findViewById(R.id.bottomLab);

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        homepage.setChecked(true);
        transaction.replace(R.id.pageFragment,new HomeFragment());
        transaction.commit();
        bottomLab.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();

                switch (radioGroup.getCheckedRadioButtonId()){
                    case R.id.homepage:
                        transaction.replace(R.id.pageFragment,new HomeFragment());
                        break;
                    case R.id.shop:
                        transaction.replace(R.id.pageFragment,new ShopFragment());
                        break;
                    case R.id.material:
                        transaction.replace(R.id.pageFragment,new MaterialFragment());
                        break;
                    case R.id.mine:
                        transaction.replace(R.id.pageFragment,new MineFragment());
                        break;
                }
                transaction.commit();
            }
        });
    }
}
