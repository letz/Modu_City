package week1.bw.com.modu_city.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import week1.bw.com.modu_city.Mine.View.HeadZoomScrollView;
import week1.bw.com.modu_city.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class MineFragment extends Fragment {


    private ImageView iv;
    private HeadZoomScrollView dzsv;
    private View view;

    public MineFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_mine, container, false);
        initView();
        return view;
    }

    private void initView() {

    }
}
