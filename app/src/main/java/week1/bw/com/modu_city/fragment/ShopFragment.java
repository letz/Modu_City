package week1.bw.com.modu_city.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;



import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import week1.bw.com.modu_city.R;
import week1.bw.com.modu_city.activity.SearchActivity;


public class ShopFragment extends Fragment {
    @BindView(R.id.dingwei_tv)
    TextView dingweiTv;
    @BindView(R.id.R1)
    RelativeLayout R1;
    @BindView(R.id.R2)
    RelativeLayout R2;
    Unbinder unbinder;
    private View view;
    private Intent intent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_shop, container, false);
        unbinder = ButterKnife.bind(this, view);



        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.R1, R.id.R2})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.R1:
                break;
            case R.id.R2:
                intent = new Intent(getContext(),SearchActivity.class);
                startActivity(intent);
                break;
        }
    }
}
