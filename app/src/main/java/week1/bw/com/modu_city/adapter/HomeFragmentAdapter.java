package week1.bw.com.modu_city.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.paradoxie.autoscrolltextview.VerticalTextview;
import com.youth.banner.Banner;
import com.youth.banner.Transformer;

import java.util.ArrayList;
import java.util.List;

import week1.bw.com.modu_city.R;
import week1.bw.com.modu_city.entity.FoodBean;
import week1.bw.com.modu_city.tools.GlideImageLoader;

public class HomeFragmentAdapter extends RecyclerView.Adapter {
    /**
     *  广告条幅类型
     * */
    public static final int BANNER = 0;
    /**
     *  通告
     * */
    public static final int ADVERTISING  = 1;
    /**
     *  分割线
     * */
    public static final int LINE = 2;
    /**
     *  频道
     * */
    public static final int CHANNEL = 3;
    /**
     *  分割线
     * */
    public static final int LINE1 = 4;
    /**
     *  服务
     * */
    public static final int SERVE = 5;
    public static final int SERVE2 = 6;
    public static final int SERVE3 = 7;
    /**
     *  上下文
     * */
    private final Context mContext;

    /**
     *  当前类型
     * */
    private int currentType = BANNER;
    private final LayoutInflater layoutInflater;
    /**
     *  油焖大虾的数据
     * */
    private List<FoodBean.DataBean> data;
    /**
     *  上下滚动条数据
     * */
    private ArrayList<String> titleList = new ArrayList<String>();

    public HomeFragmentAdapter(Context mContext, List<FoodBean.DataBean> data) {
        this.mContext = mContext;
        layoutInflater = LayoutInflater.from(mContext);
        this.data = data;
    }

    /**
     *   相当于getView
     *   创建ViewHolder
     * */
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if(i == BANNER){
            Log.e("LSG","Create");
            return new BannerViewHolder(mContext,layoutInflater.inflate(R.layout.banner_view,null));
        }else if(i == CHANNEL){
            return new ChannelViewHolder(mContext,layoutInflater.inflate(R.layout.channel_view,null));
        }else if(i == LINE || i == LINE1){
            return new LineViewHolder(mContext,layoutInflater.inflate(R.layout.line_view,viewGroup,false));
        }else if(i == SERVE || i == SERVE2 || i == SERVE3){
            return new ServiceViewHolder(mContext,layoutInflater.inflate(R.layout.service_view,null));
        }else if(i == ADVERTISING){
            return new AdvertisingViewHolder(mContext,layoutInflater.inflate(R.layout.advertising_view,null));
        }

        return null;
    }
    /**
     *  相当于getView中的绑定数据模块
     * */
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if(getItemViewType(i) == BANNER){
            BannerViewHolder bannerViewHolder = (BannerViewHolder) viewHolder;
        }else if(getItemViewType(i) == CHANNEL){
            ChannelViewHolder channelViewHolder = (ChannelViewHolder) viewHolder;
        }else if(getItemViewType(i) == LINE || getItemViewType(i) == LINE1){
            LineViewHolder lineViewHolder = (LineViewHolder) viewHolder;
        }else if(getItemViewType(i) == SERVE || getItemViewType(i) == SERVE2 || getItemViewType(i) == SERVE3){
            ServiceViewHolder serviceViewHolder = (ServiceViewHolder) viewHolder;
        }else if(getItemViewType(i) == ADVERTISING){
            AdvertisingViewHolder advertisingViewHolder = (AdvertisingViewHolder) viewHolder;
        }
    }

    /**
     *  得到的类型
     * */
    @Override
    public int getItemViewType(int position) {
        switch (position){
            case BANNER:
                currentType = BANNER;
                break;
            case ADVERTISING:
                currentType = ADVERTISING;
                break;
            case CHANNEL:
                currentType = CHANNEL;
                break;
            case LINE:
                currentType = LINE;
                break;
            case LINE1:
                currentType = LINE1;
                break;
            case SERVE:
                currentType = SERVE;
                break;
            case SERVE2:
                currentType = SERVE2;
                break;
            case SERVE3:
                currentType = SERVE3;
                break;
        }
        return currentType;
    }

    /**
     *   总共有多少个item
     * */
    @Override
    public int getItemCount() {
        // 开发过程中从1-->4
        return 8;
    }

    /**
     *  广告条幅Banner的ViewHolder
     * */
    private class BannerViewHolder extends RecyclerView.ViewHolder {
        private Context mContext;
        private Banner banner;
        public BannerViewHolder(Context mContext, @NonNull View itemView) {
            super(itemView);
            this.mContext = mContext;
            this.banner = itemView.findViewById(R.id.banner);
            setData();
        }

        public void setData(){
            List<Integer> images = new ArrayList<>();
            images.add(R.drawable.music1);
            images.add(R.drawable.music2);
            images.add(R.drawable.music3);
            images.add(R.drawable.music4);
            images.add(R.drawable.music5);
            images.add(R.drawable.music6);
            banner.setImages(images);
            banner.setImageLoader(new GlideImageLoader());
            banner.setIndicatorGravity(Gravity.RIGHT);
            banner.setBannerAnimation(Transformer.RotateUp);
            banner.start();
        }
    }

    /**
     *  通告Advertising的ViewHolder
     * */
    private class AdvertisingViewHolder extends RecyclerView.ViewHolder{
        private VerticalTextview text;
        private Context mContext;
        public AdvertisingViewHolder(Context mContext, @NonNull View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.text);
            this.mContext = mContext;
            init();
        }

        private void init() {
            titleList.add("想跑iOS 模拟器, 还是换苹果电脑开发吧");
            titleList.add("我是丑人脸上的鼻涕你发出完美的声音我被留在这里");
            titleList.add("我被默默揩去你冷酷外表下藏着诗情画意");
            titleList.add("我已经够胖还吃东西你踏着七彩祥云离去");
            text.setTextList(titleList);
            text.setText(15,20, Color.BLACK);
            text.setAnimTime(1000);  // 进入退出时间间隔
            text.setTextStillTime(3000);   // 停留时间
            text.setOnItemClickListener(new VerticalTextview.OnItemClickListener() {
                @Override
                public void onItemClick(int i) {
                    Toast.makeText(mContext, titleList.get(i)+"", Toast.LENGTH_SHORT).show();
                }
            });
            text.startAutoScroll();
        }
    }

    /**
     *  分割线LINE的ViewHolder
     * */
    private class LineViewHolder extends RecyclerView.ViewHolder{
        private Context mContext;
        public LineViewHolder(Context mContext, @NonNull View itemView) {
            super(itemView);
            this.mContext = mContext;
        }
    }


    /**
     *  频道Channel的ViewHolder
     * */
    private class ChannelViewHolder extends RecyclerView.ViewHolder{
        private Context mContext;
        private GridView gv;
        private ChannelAdapter adapter;

        public ChannelViewHolder(Context mContext,@NonNull View itemView) {
            super(itemView);
            this.mContext = mContext;
            gv = itemView.findViewById(R.id.gv_channel);
            setData();
        }

        private void setData() {
//            Log.i("aa",data.size()+"");
            adapter = new ChannelAdapter(mContext,data);
            gv.setAdapter(adapter);
        }
    }

    /**
     *
     *  服务Service的ViewHolder
     * */
    class ServiceViewHolder extends RecyclerView.ViewHolder{
        private Context mContext;
        private TextView service_name;
        private RecyclerView horizontal_rv;
        private ServiceAdapter adapter;

        public ServiceViewHolder(Context mContext, @NonNull View itemView) {
            super(itemView);
            this.mContext = mContext;
            service_name = itemView.findViewById(R.id.service_name);
            horizontal_rv = itemView.findViewById(R.id.horizontal_rv);
            initData();
        }

        private void initData() {
            service_name.setText(data.get(1).getTitle());
            adapter = new ServiceAdapter(data,mContext);
            LinearLayoutManager manager = new LinearLayoutManager(mContext);
            manager.setOrientation(LinearLayoutManager.HORIZONTAL);
            horizontal_rv.setLayoutManager(manager);
            horizontal_rv.setAdapter(adapter);
        }

    }
}
