package week1.bw.com.modu_city.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import week1.bw.com.modu_city.Material.Adapter.MyFragmentAdapter;
import week1.bw.com.modu_city.Material.Fragment.Cate_Fragment;
import week1.bw.com.modu_city.Material.Fragment.Funny_Fragment;
import week1.bw.com.modu_city.Material.Fragment.Invest_Fragment;
import week1.bw.com.modu_city.Material.Fragment.Movie_Fragment;
import week1.bw.com.modu_city.Material.Fragment.Play_Fragment;
import week1.bw.com.modu_city.R;

public class MaterialFragment extends Fragment {

    private View view;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private List<Fragment> list = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_material, null);
        initData();
        init();
        return view;
    }

    private void init(){
        tabLayout = view.findViewById(R.id.use_fragment_tabLayout);
        viewPager = view.findViewById(R.id.use_fragment_viewPager);
        MyFragmentAdapter myFragmentAdapter = new MyFragmentAdapter(getActivity().getSupportFragmentManager(),list);
        viewPager.setAdapter(myFragmentAdapter);
        viewPager.setCurrentItem(0);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void initData(){
        //招商界面
        list.add(new Invest_Fragment());
        //搞笑界面
        list.add(new Funny_Fragment());
        //美食界面
        list.add(new Cate_Fragment());
        //电影界面
        list.add(new Movie_Fragment());
        //爱玩界面
        list.add(new Play_Fragment());
    }

}
