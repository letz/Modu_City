package week1.bw.com.modu_city.net;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import week1.bw.com.modu_city.IGetData;
import week1.bw.com.modu_city.entity.FoodBean;


public class RetrofitGetData {
    private IGetData iGetData;

    public RetrofitGetData(IGetData iGetData) {
        this.iGetData = iGetData;
    }

    public void getDataAsync(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.qubaobei.com/ios/cf/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RetrofitService retrofitService = retrofit.create(RetrofitService.class);
        Call<FoodBean> data = retrofitService.getData("http://www.qubaobei.com/ios/cf/dish_list.php?stage_id=1&limit=20&page=1");
        data.enqueue(new Callback<FoodBean>() {
            @Override
            public void onResponse(Call<FoodBean> call, Response<FoodBean> response) {
                FoodBean body = response.body();
                iGetData.getString(body);
//                Log.i("aa",body.toString());
            }

            @Override
            public void onFailure(Call<FoodBean> call, Throwable t) {

            }
        });
    }

}
