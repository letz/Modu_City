package week1.bw.com.modu_city.Material.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import week1.bw.com.modu_city.Material.Enity.Bean;
import week1.bw.com.modu_city.R;
import week1.bw.com.modu_city.activity.DetailsActivity;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{
    private List<Bean.DataBean> list;
    private String site;
    private Context context;
    private MyAdapter myAdapter;


    public void setMyAdapter(MyAdapter myAdapter) {
        this.myAdapter = myAdapter;
    }

    public MyAdapter(List<Bean.DataBean> list, String site) {
        this.list = list;
        this.site = site;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, null);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        context = viewGroup.getContext();
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {
        myViewHolder.tv.setText(list.get(i).getTitle());
        myViewHolder.site.setText(site);
        myViewHolder.num.setText("  "+list.get(i).getNum());
        Picasso.with(context).load(list.get(i).getPic()).into(myViewHolder.pic1);
        Picasso.with(context).load(list.get(i).getPic()).into(myViewHolder.pic2);
        Picasso.with(context).load(list.get(i).getPic()).into(myViewHolder.pic3);
        myViewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                list.remove(i);
                Toast.makeText(context, "你已成功删除该数据", Toast.LENGTH_SHORT).show();
                myAdapter.notifyDataSetChanged();
                return false;
            }
        });
        
        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "成功跳转页面", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context,DetailsActivity.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tv,site,num;
        ImageView pic1,pic2,pic3;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv = itemView.findViewById(R.id.item_tv);
            site = itemView.findViewById(R.id.item_site);
            num = itemView.findViewById(R.id.item_num);
            pic1 = itemView.findViewById(R.id.item_pic1);
            pic2 = itemView.findViewById(R.id.item_pic2);
            pic3 = itemView.findViewById(R.id.item_pic3);
        }
    }
}
