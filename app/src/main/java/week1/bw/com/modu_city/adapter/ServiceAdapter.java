package week1.bw.com.modu_city.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import week1.bw.com.modu_city.R;
import week1.bw.com.modu_city.entity.FoodBean;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ServiceViewHolder> {
    private List<FoodBean.DataBean> data = new ArrayList<>();
    private Context mContext;

    public ServiceAdapter(List<FoodBean.DataBean> data, Context mContext) {
        this.data = data;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ServiceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_service, viewGroup,false);
        return new ServiceViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceViewHolder serviceViewHolder, int i) {
        String title = data.get(i).getTitle();
        String substring = title.substring(0, 2);
        serviceViewHolder.tv_service.setText(substring);
        Glide.with(mContext).load(data.get(i).getPic())
                .apply(RequestOptions.bitmapTransform(new CircleCrop()))
                .into(serviceViewHolder.iv_service);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ServiceViewHolder extends RecyclerView.ViewHolder{
        private ImageView iv_service;
        private TextView tv_service;

        public ServiceViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_service = itemView.findViewById(R.id.iv_service);
            tv_service = itemView.findViewById(R.id.tv_service);
        }
    }
}
