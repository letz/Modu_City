package week1.bw.com.modu_city.net;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;
import week1.bw.com.modu_city.entity.FoodBean;

public interface RetrofitService {
    @GET()
    Call<FoodBean> getData(@Url String url);
}
